<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Esa - JCC - Form</title>
</head>
<body>
    <h2>Buat Account Baru!</h2>
    <h3>Sign Up Form</h3>
    
    <form action="/welcome" method="POST">
        @csrf
        <label for="fname">First Name :</label><br><br>
        <input type="text" name="fname" id="fname" autocomplete="off"><br><br>

        <label for="lname">Last Name :</label><br><br>
        <input type="text" name="lname" id="lname" autocomplete="off"><br><br>

        <label for="gender">Gender :</label><br><br>
        <input type="radio" name="gender" value="Male">Male <br>
        <input type="radio" name="gender" value="Female">Female <br>
        <input type="radio" name="gender" value="Other">Other <br>
        <br>

        <label for="nationality">Nationality :</label><br><br>
        <select name="nationality" id="nationality">
            <option value="Indonesian">Indonesian</option>
            <option value="Singaporean">Singaporean</option>
            <option value="Malaysian">Malaysian</option>
            <option value="Indian">Indian</option>
        </select>
        <br><br>

        <label for="language">Language Spoken :</label><br><br>
        <input type="checkbox" name="language">Bahasa Indonesia <br>
        <input type="checkbox" name="language">English <br>
        <input type="checkbox" name="language">Other <br>
        <br>

        <label for="bio">Bio :</label><br><br>
        <textarea name="bio" id="bio" cols="20" rows="6"></textarea>
        <br>

        <input type="submit" value="Sign Up">
    </form>

</body>
</html>