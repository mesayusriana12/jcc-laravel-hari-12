<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('auth.register');
    }

    public function welcome(Request $request)
    {
        return view('auth.welcome',[
            'fname' => $request->fname,
            'lname' => $request->lname
        ]);
    }
}
